import cv2
import numpy as np
min_thres = 0.65
# to keep overlap, set nms_thres to 1.
# the lower the shreshold number, the more
# duplicates are likely to be removed.
nms_thres = 0.35
webcam = cv2.VideoCapture(0)
webcam.set(3, 1280)
webcam.set(4, 720)
webcam.set(10, 150)

classFile = 'coco.names'

with open(classFile, 'rt') as f:
    classNames = f.read().rstrip('\n').split('\n')


configPath = 'ssd_mobilenet_v3_large_coco_2020_01_14.pbtxt'
weightsPath = 'frozen_inference_graph.pb'

net = cv2.dnn_DetectionModel(weightsPath, configPath)
net.setInputSize(320, 320)
net.setInputScale(1.0/127.5)
net.setInputMean((127.5, 127.5, 127.5))
net.setInputSwapRB(True)

# Sets the threshold of detection to 50% and resturns the bounding
# box of the detected object.
while True:
    success, img = webcam.read()
    classIds, confs, bbox = net.detect(img, confThreshold=min_thres)
    bbox = list(bbox)
    confs = list(np.array(confs).reshape(1, -1)[0])
    confs = list(map(float, confs))

    # Returns the indices of the non-maximum supression of bounding boxes.
    indices = cv2.dnn.NMSBoxes(bbox, confs, min_thres, nms_threshold=nms_thres)
    #print(indices) 

    for i in indices:
        if len(classIds) != 0:
            box = bbox[i]
            x,y,w,h = box[0], box[1], box[2], box[3]
            cv2.rectangle(img, (x,y), (x+w, h+y), color=(0, 255, 0), thickness=2)
            cv2.putText(img, classNames[classIds[i]-1].upper(), (box[0]+10, box[1]+30),
                    cv2.FONT_HERSHEY_COMPLEX, 1, (0,255,0), 2)

    cv2.imshow("Output", img)
    if cv2.waitKey(1) == ord('q'):
        break
