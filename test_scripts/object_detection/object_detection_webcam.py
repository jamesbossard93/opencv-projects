import cv2

min_thres = 0.65
webcam = cv2.VideoCapture(0)
webcam.set(3, 640)
webcam.set(4, 480)


classFile = 'coco.names'

with open(classFile, 'rt') as f:
    classNames = f.read().rstrip('\n').split('\n')


configPath = 'ssd_mobilenet_v3_large_coco_2020_01_14.pbtxt'
weightsPath = 'frozen_inference_graph.pb'

net = cv2.dnn_DetectionModel(weightsPath, configPath)
net.setInputSize(320, 320)
net.setInputScale(1.0/127.5)
net.setInputMean((127.5, 127.5, 127.5))
net.setInputSwapRB(True)

# Sets the threshold of detection to 50% and resturns the bounding
# box of the detected object.
while True:
    success, img = webcam.read()
    classIds, confs, bbox = net.detect(img, confThreshold=min_thres)
    
    # Makes sure there is at least one object being detected.
    if len(classIds) != 0:
        # Adds a bounding box and name to the detected object.
        for classId, confidence, box in zip(classIds.flatten(), confs.flatten(), bbox):
            cv2.rectangle(img, box, color=(0, 255, 0), thickness=2)
            cv2.putText(img, classNames[classId-1].upper(), (box[0]+10, box[1]+30),
                    cv2.FONT_HERSHEY_COMPLEX, 1, (0,255,0), 2)

    cv2.imshow("Output", img)
    cv2.waitKey(1)
