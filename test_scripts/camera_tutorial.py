import numpy as np
import cv2

# Starts recording from first webcam/camera.
cap = cv2.VideoCapture(0)

while True:
    # Captures webcam footage through cap.read(),
    # then shows the image through cv2.imshow().
    ret, frame = cap.read()

    cv2.imshow('frame', frame)

    if cv2.waitKey(1) == ord('q'):
        break
# Dismounts webcam and closes window.
cap.release()
cv2.destroyAllWindows()

